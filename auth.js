const jwt = require("jsonwebtoken");

// For security measures, A unique identifier token
const secret = "EcommerceAPI";

// Creating Token
module.exports.createAccessToken = (user) => {
  const data = {
      id: user._id,
      email : user.email,
      isAdmin : user.isAdmin
  }
  return jwt.sign(data, secret, {
      // expiresIn: 1200 - for session timeout
  });
};

// Token Verification
module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token !== "undefined") {
      console.log(token);
      token = token.slice(7, token.length);
      return jwt.verify(token, secret, (err, data) => {
          if (err) {
              return res.send({auth : "failed"});
          } else {
              next(); 
          };
      });
  } else {
      return res.send({auth : "failed"});
  };
};


// Token Decryption
module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
      console.log(token);
      token = token.slice(7, token.length);
      return jwt.verify(token, secret, (err, data) => {
          if (err) {
              return null;
          } else {
              return jwt.decode(token, {complete:true}).payload;
          };  
      });
  } else {
      return null;
  };
};
