const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require("../auth");

// User Registration Controller
module.exports.registerAdmin = async (reqBody) => {
	try {
		if (!reqBody.email.includes("@")) {
			return {
				message: 'Please enter a valid email address!'
			};
		};
		if (reqBody.password.length < 7) {
			return {
				message: 'Password should be atleast 7 characters!'
			};
		};
		if (!/[A-Z]/.test(reqBody.password)) {
			return {
				message: 'Password should contain at least one uppercase letter!'
			};
		};
		const result = await User.findOne({ email: reqBody.email });
		if (result) {
			return {
				message: 'Email already exist. Try another one!'
			};
		} else {
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				password: bcrypt.hashSync(reqBody.password, 10),
				isAdmin: true
			});
			const user = await newUser.save();
			return {
				message: "Admin Registered successfully"
			};
		}
	} catch (err) {
		return false;
	}
};

// Admin login Controller
module.exports.loginAdmin = async (reqBody) => {
	const result = await User.findOne({ email: reqBody.email });
	if (result == null) {
		return {
			message: "Admin doesn't exist!"
		};
	};
		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
		if (isPasswordCorrect) {
			return {
				message: `Welcome Admin ${result.firstName}`,
				token: auth.createAccessToken(result)
			};
		}
		return {
			message: 'Password Incorrect!'
		};
	};

// Set User as Admin - Admin Only
module.exports.makeAdmin = async (reqParams, data) => {
	try {
		if (data.isAdmin) {
			let updatedStatus = {
				isAdmin: true
			};
			await User.findByIdAndUpdate(reqParams.id, updatedStatus);
			return {
				message: `This user is now an admin.`
			}
		} else {
			return {
				message: 'Only admins can set user to admin!'
			}
		}
	} catch (err) {
		return err;
	};
};