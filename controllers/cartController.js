const Cart = require("../models/Cart");
const Product = require('../models/Product');
const Order = require("../models/Order");
const auth = require("../auth");

// Add to Cart - Users only
module.exports.addToCart = async (reqBody, userId, data) => {
    try {
        if (data.isAdmin == false) {
            let subtotal = await SubTotal(reqBody);
            const addedToCart = new Cart({
                userId: userId,
                products: reqBody.products,
                subtotal: subtotal
            })
            await addedToCart.save();
            return {
                message: `Successfully added to your cart!`
            };

        } else {
            return {
                message: "Admin not allowed!"
            };
        };
    } catch (err) {
        return err;
    };
};

//   Getting the Subtotal for each item
const SubTotal = async (data) => {
    if (data.products.length === 0) {
        return 0;
    };
    let amount = 0;
    for (const product of data.products) {
        try {
            const foundProduct = await Product.findById(product.productId);
            amount += foundProduct.price * product.quantity;
        } catch (err) {
            return err;
        };
    };
    return amount;
};

// Show all items in cart who is loggedIn
module.exports.itemsInCart = async (userId) => {
    try {
        const items = await Cart.find({ userId: userId });
        return items;
    } catch (err) {
        return err;
    }
};

// CheckOut from cart
module.exports.checkout = async (cartId, data) => {
    try {
        if (data.isAdmin == false) {
            const cart = await Cart.findById(cartId.cartId);
            if (!cart) {
                return {
                    message: 'Not found in the cart!'
                }
            }
            const newOrder = new Order({
                userId: cart.userId,
                products: cart.products,
                totalAmount: cart.subtotal
            });
            await newOrder.save();
            await Cart.findByIdAndRemove(cart);
            return {
                message: 'Successfully created an order'
            };
        } else {
            return {
                message: "Not Autorized"
            }
        }
    } catch (err) {
        return err
    }
}


// Deleting Product from the cart
module.exports.remove = async (productId, data) => {
    try {
        if (data.isAdmin === false) {
            await Cart.findByIdAndRemove(productId);
            return {
                message: 'Product removed successfully'
            };
        } else {
            return {
                message: 'Admins not allowed!'
            };
        };
    } catch (err) {
        return err;
    };
};
