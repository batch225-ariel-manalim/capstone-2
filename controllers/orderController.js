const Order = require("../models/Order");
const Product = require('../models/Product');

// Placing an Order directly - Users only
module.exports.createOrder = async (reqBody, userId, data) => {
  try {
      let totalAmount = await TotalAmount(reqBody);
      const placedOrder = new Order({
        userId: userId,
        products: reqBody.products,
        totalAmount: totalAmount
      })
      await placedOrder.save();
      return {
        message: 'Order Created Successfully!'
      };
  } catch (err) {
    return err;
  };
};

// Getting the total amount
const TotalAmount = async (data) => {
  if (data.products.length === 0) {
    return 0;
  };
  let amount = 0;
  for (const product of data.products) {
    try {
      const foundProduct = await Product.findById(product.productId);
      amount += foundProduct.price * product.quantity;
    } catch (err) {
      return err;
    };
  };
  return amount;
};

// Retrieve authenticated user's order
module.exports.myOrders = async (userId) => {
  try {
      const orders = await Order.find({ userId: userId });
      return orders;
  } catch (err) {
      return err;
  }
};

// Show All Orders -Admin Only
module.exports.showAllOrders = async (data) => {
  try {
    if (data.isAdmin) {
      const result = await Order.find({});
      return result;
    } else {
      return {
        message: 'Sorry! Only admins can use this feature!'
      };
    };
  } catch (err) {
    return err;
  };
};
