const Product = require("../models/Product");

// Creating a Product - admin only
module.exports.createProduct = async (data) => {
  let newProduct = new Product({
    name: data.product.name,
    description: data.product.description,
    price: data.product.price,
    qty: data.product.qty,
    // image: data.image
  });
  await newProduct.save();
  return {
    message: `${data.product.name} has been listed!`
  };
}

module.exports.getAllProducts = async () => {
  const result = await Product.find({});
  return result;
}

// Retrieving active products
module.exports.activeProducts = async () => {
  const result = await Product.find({ isActive: true });
  return result;
};

// Retrieving archived products
module.exports.archivedProducts = async () => {
  const result = await Product.find({ isActive: false });
  return result;
};

// Retrieving single product
module.exports.getProduct= (data) => {
  return Product.findById(data.id).then(result => {
    return result;
  }) ;
}

// // Updating Product information - Admin only
module.exports.updateProduct = async (reqParams, reqBody, data) => {
  try {
      let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        qty: reqBody.qty
      };
      await Product.findByIdAndUpdate(reqParams.id, updatedProduct);
      return {
        message: `${updatedProduct.name} updated successfully!`
      };
  } catch (err) {
    return err;
  };
}

// Archiving Product - Admin only
module.exports.archiveProduct = async (productId, data) => {
  try {
      await Product.findByIdAndUpdate(productId, data);
      return {
        message: `Product archived successfully.`
      };
  } catch (err) {
    return err;
  };
};

// Activate Product - Admin only
module.exports.activateProduct = async (productId, data) => {
  try {
      await Product.findByIdAndUpdate(productId, data);
      return {
        message: `Product activated successfully.`
      };
  } catch (err) {
    return err;
  };
};

// Deleting Product - Admin Only
module.exports.deleteProduct = async (productId, data) => {
  try {
    if (data.isAdmin) {
      await Product.findByIdAndRemove(productId);
      return {
        message: 'Product deleted successfully'
      };
    } else {
      return {
        message: 'Only admins can delete a product!'
      };
    };
  } catch (err) {
    return err;
  };
};
