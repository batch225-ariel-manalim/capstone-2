const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		name: reqBody.name,
		email: reqBody.email,
		mobile: reqBody.mobile,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return user
		};
	});
};

// User authentication
module.exports.loginUser = async (reqBody) => {
	const result = await User.findOne({ email: reqBody.email });
	if (result == null) {
		return {
			message: "User doesn't exist!"
		};
	}
	const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
	if (isPasswordCorrect) {
		return {
			message: `A user has logged in!`,
			access: auth.createAccessToken(result)
		};
	} else {
		return {
			message: "Password Incorrect!"
		};
	};
};


// Retrieving User Details
module.exports.getUserDetails = async (data) => {
	try {
		return data
	} catch (err) {
		return err
	};
};



module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {
		if (result.length > 0) {
			return true
		}
		return false
	})
}

module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({productId : data.productId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
	});
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({userId : data.userId});
		return product.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});
	if(isUserUpdated && isProductUpdated){
		return {
			message: "You're enrolled"
		}
	} else {
		return false;
	};
}