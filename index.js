// Connections
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();

// Connect Express
const app = express();
const port = 4001;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// To limit access to our application. We can allow/disallow certain applications from accessing our app
app.use(cors());


// Connect to Mongoose
mongoose.connect(`mongodb+srv://ayei20:${process.env.PASSWORD}@cluster0.dxlfk3e.mongodb.net/ecommerce-API?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

//Testing mongoDB connection
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("MongoDB Connected!"));

// Routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cartRoutes = require("./routes/cartRoutes");
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/order", orderRoutes);
app.use("/cart", cartRoutes);


app.listen(port, () => console.log(`LISTENING ON PORT ${port}`));