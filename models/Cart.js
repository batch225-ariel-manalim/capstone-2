const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'UserID is Required.']
    },
    products: [
        {
            productId: {
                type: String,
                required: [true, 'The Product ID is required']
            },
            quantity: {
                type: Number,
                required: [true, 'The Quantity is required.']
            }
        }
    ],

    subtotal: {
        type: Number
    }
})

module.exports = mongoose.model('Cart', cartSchema)
