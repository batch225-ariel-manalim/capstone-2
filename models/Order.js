const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'UserID is Required.']
    },
    products: [
        {
            productId: {
                type: String,
                required: [true, 'The Product ID is required']
            },
            quantity: {
                type: Number,
                required: [true, 'The Quantity is required.']
            }
        }
    ],

    totalAmount: {
        type: Number,
    },
    modeOfPayment: {
        type: String,
        default:"COD"
    },
    purchaseOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Order', orderSchema)

