const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "The Name of the product is required"],
    unique: true,
  },
  description: {
    type: String,
    required: [true, "Description is required"],
  },
  price: {
    type: String,
    required: [true, "Price is required."],
  },
  qty: {
    type: String,
    required: [true, "Qty is required."],
  },
  // image: {
  //   type: String,
  // },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  orders : [
		{
			userId : {
				type : String,
				required: [true, "UserId is required"]
			},
			purchashedOn : {
				type : Date,
				default : new Date() 
			}
		}
	]

});

module.exports = mongoose.model("Product", productSchema);

