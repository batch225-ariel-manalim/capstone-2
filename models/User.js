const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Last Name is required."]
  },
  email: {
    type: String,
    required: [true, "Email is required."]
  },
  password: {
    type: String,
    required: [true, "Password is required."]
  },
  mobile: {
    type: String,
    required: [true, "Mobile Number is required."]
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  // orders: [
  //   {
  //     productId : {
  //       type: String,
  //       required : [true, "Product ID is required"]
  //     },
  //     enrolledOn : {
  //       type: Date,
  //       default: new Date()
  //     },
  //     status : {
  //       type: String,
  //       default: "Enrolled"
  //     }
  //   }

  // ]
});

module.exports = mongoose.model("User", userSchema);
