const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartController");
const auth = require("../auth");

// Add to cart
router.post('/addToCart', auth.verify, (req, res) => {
  const data = {
    order: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  const userId = auth.decode(req.headers.authorization).id;
  cartController.addToCart(req.body, userId, data).then((result) => {
    res.send(result)
  })
})

// Show all items in cart for a specific user
router.get('/itemsInCart', auth.verify, (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  cartController.itemsInCart(userId).then((result) => {
    res.send(result);
  });
});

// Checkout from cart
router.post('/checkout', auth.verify, (req, res) => {
  const cartId = ({ cartId: req.body.id });
  const data = {
    order: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  cartController.checkout(cartId, data).then((result) => {
    res.send(result)
  })
})

// Deleting Product from the cart
router.delete('/remove/:id', auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  cartController.remove(req.params.id, data).then((result) => {
    res.send(result)
  })
})
module.exports = router;
