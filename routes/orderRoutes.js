const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");
const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const token = req.header('Authorization').replace('Bearer ', '');

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = decoded;
    next();
  } catch (error) {
    console.error(error);
    res.status(401).json({ message: 'Unauthorized' });
  }
};
// create order directly
router.post('/checkout', (req, res) => {
  const data = {
    order: req.body,
  };
  const userId = auth.decode(req.headers.authorization).id;
  orderController.createOrder(req.body, userId, data).then((result) => {
    res.send(result)
  });
});

// Retrieve authenticated user's order
router.get('/myOrders', (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  orderController.myOrders(userId).then((result) => {
    res.send(result);
  });
});

// Show All Orders -Admin Only
router.get('/showAllOrders', auth.verify, (req, res) => {
  const data = {
    order: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  orderController.showAllOrders(data).then((result) => {
    res.send(result)
  })
})

module.exports = router;

