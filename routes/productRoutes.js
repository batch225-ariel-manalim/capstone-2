const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");
// const multer = require('multer');

// // Create Storage for Images
// const storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null, 'uploads/')
//   },
//   filename: function (req, file, cb) {
//     cb(null, file.fieldname + '-' + Date.now())
//   }
// })

// // Set Upload and Rename Image function
// const upload = multer({ storage: storage, fileFilter: function (req, file, cb) {
//   // Validate File Type
//   if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
//     return cb(new Error('Only image files are allowed!'))
//   }
//   cb(null, true)
// }}).single('image')

// Listing Product - Admin Only
// router.post("/addProduct", (req, res) => {
//   upload(req, res, async function (err) {
//     if (err instanceof multer.MulterError) {
//       // A Multer error occurred when uploading.
//       return res.status(500).json({ message: "Error uploading image" })
//     } else if (err) {
//       // An unknown error occurred when uploading.
//       return res.status(500).json({ message: "Error uploading image" })
//     }
//     // Everything is fine.
//     try {
//       const result = await productController.createProduct({
//         name: req.body.name,
//         description: req.body.description,
//         price: req.body.price,
//         qty: req.body.qty,
//         image: req.file.filename
//       });
//       res.status(200).json(result);
//     } catch (error) {
//       console.log(error);
//       res.status(500).json({ message: "Error creating product" });
//     }
//   })
// });

router.post('/addProduct',(req, res) => {
  const data = {
		product: req.body
	}
  productController.createProduct(data).then((result) => {
    res.send(result);
  })
})

router.get("/", (req, res) => {
  productController.getAllProducts().then(result => res.send(result));

});

// Retrieving All active products
router.get("/activeProducts", (req, res) => {
  productController.activeProducts().then((result) => {
    res.send(result);
  });
}
);
// Retrieving All archived products
router.get("/archivedProducts", (req, res) => {
  productController.archivedProducts().then((result) => {
    res.send(result);
  });
}
);

// Retrieving single product
router.get("/:id", (req, res) => {
	productController.getProduct(req.params, req.body).then(result => res.send(result));
});


// Updating Product information - Admin only
router.put("/update/:id", (req, res) => {
  const data = {
    product: req.body
  };
  productController.updateProduct(req.params, req.body, data).then(result => res.send(result));
});

// Archiving Product - Admin only
router.put("/archive/:id", (req, res) => {
  const productId = req.params.id;
  const data = {
    isActive: false
  };
  productController.archiveProduct(productId, data).then((result) => {
    res.send(result);
  });
});

// Unarchiving Product - Admin only
router.put("/activate/:id", (req, res) => {
  const productId = req.params.id;
  const data = {
    isActive: true
  };
  productController.activateProduct(productId, data).then((result) => {
    res.send(result);
  });
});

// Deleting Product - Admin Only
router.delete('/delete/:id', auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  productController.deleteProduct(req.params.id, data).then((result) => {
    res.send(result)
  })
})

module.exports = router;