const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const adminController = require("../controllers/adminController");
const auth = require("../auth");
const User = require("../models/User");

// User Registration Route
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});

// Route for user login authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})

// Admin Registration Route
router.post("/registerAdmin", (req, res) => {
  adminController.registerAdmin(req.body).then(result => res.send(result));
});

// Route for admin login authentication
router.post("/loginAdmin", (req, res) => {
  adminController.loginAdmin(req.body).then(result => res.send(result));
});

// get autheticated user details
router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization)
	userController.getUserDetails(userData).then((result) => {
		res.send(result);
	});
});



// Set User as Admin - Admin Only
router.patch("/makeAdmin/:id", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  adminController.makeAdmin(req.params, data).then((result) => {
    res.send(result);
  });
});


// Check Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

// //checkout
// router.post("/checkoutMe", (req, res) => {

// 	let data = {
// 		// user ID will be retrieved from the request header
// 		userId : auth.decode(req.headers.authorization).id,

// 		// Course ID will be retrieve from the request body
// 		productId : req.body.productId
// 	}
//   console.log(data)
// 	userController.checkout(data).then(result => res.send(result));
// })

module.exports = router;
